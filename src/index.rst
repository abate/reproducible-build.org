.. reproducible-build.org documentation master file, created by
   sphinx-quickstart on Tue Nov  1 17:50:11 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to reproducible-build.org's documentation!
==================================================


The Project
-----------

Whilst anyone may inspect the source code of free and open source software for malicious flaws, most software is distributed pre-compiled with no method to confirm whether they correspond.

This incentivises attacks on developers who release software, not only via traditional exploitation, but also in the forms of political influence, blackmail or even threats of violence.

This is particularly a concern for developers collaborating on privacy or security software: attacking these typically result in compromising particularly politically-sensitive targets such as dissidents, journalists and whistleblowers, as well as anyone wishing to communicate securely under a repressive regime.

Whilst individual developers are a natural target, it additionally encourages attacks on build infrastructure as a successful attack would provide access to a large number of downstream computer systems. By modifying the generated binaries here instead of modifying the upstream source code, illicit changes are essentially invisible to its original authors and users alike.

The motivation behind the Reproducible Builds project is therefore to allow verification that no vulnerabilities or backdoors have been introduced during this compilation process. By promising identical results are always generated from a given source, this allows multiple third parties to come to a consensus on a “correct” result, highlighting any deviations as suspect and worthy of scrutiny.

This ability to notice if a developer or build system has been compromised then prevents such threats or attacks occurring in the first place, as any compromise can be quickly detected. As a result, front-liners cannot be threatened/coerced into exploiting or exposing their colleagues.

This website
------------

This website (https://reproducible-build.org/docs/) provides online developer
oriented documentation, although it also provides an high level pointers
regarding reproducible builds.

The documentation is automatically generated from the master branch of the <ADD
repo here>. To propose a patch, please clone the repository and submit a merge
request.


.. toctree::
   :maxdepth: 1
   :caption: Introduction

   docs/definition
   docs/buy_in
   docs/plans
   docs/history
   docs/publications

.. toctree::
   :maxdepth: 1
   :caption: Achieve deterministic builds

   docs/define_build_environment
   docs/achieve_deterministic_builds
   docs/distribute_the_environment
   docs/verification

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
