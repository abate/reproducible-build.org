This page is trying to document the different tools for rebuilding:

orchestrators
=============

-  https://github.com/kpcyrd/rebuilderd (Agnostic)
-  https://github.com/fepitre/package-rebuilder

rebuilder-backends
==================

-  https://github.com/archlinux/archlinux-repro (Arch Linux)
-  https://github.com/archlinux/devtools/blob/master/makerepropkg.in
   (Arch Linux)
-  https://salsa.debian.org/debian/devscripts/-/blob/master/scripts/debrebuild.pl
   (Debian)
-  https://github.com/fepitre/debrebuild (Python rewrite of the above,
   +more)
-  https://github.com/fepitre/rpmreproduce (Fedora on Qubes OS)

clients/frontends
=================

-  https://gitlab.archlinux.org/archlinux/rebuilderd-website (Agnostic)
-  https://github.com/kpcyrd/ismyarchverifiedyet (Arch Linux)
-  https://github.com/archlinux/arch-repro-status (Arch Linux)

verifiers?
==========

(I wonder how we can contrast a more thorough attestation verification
vs regular client/frontends) - https://github.com/tweag/trustix -
https://github.com/in-toto/apt-transport-in-toto -
https://github.com/fepitre/dnf-plugin-in-toto

outside the above model
=======================

for OCaml / OPAM we have some infrastructure as well - the builder
(capturing everything [installed packages, environment variables,
sources used]) is ``orb build``, the rebuilder is ``orb rebuild`` (from
<https://github.com/roburio/orb). for scheduling we’re currently using
builder (https://github.com/roburio/builder) and have a web frontend
(https://git.robur.io/robur/builder-web) – live at
https://builds.robur.coop
