
Verification
============

.. toctree::
   :maxdepth: 1

   checksums
   embedded_signatures
   sharing_certifications
   rebuilders
