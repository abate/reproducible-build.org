
Stable Outputs
==============

Data structures such as `Perl
hashes <http://perldoc.perl.org/functions/keys.html>`__, `Python
dictionaries <https://docs.python.org/2/library/stdtypes.html#mapping-types-dict>`__,
or `Ruby Hash objects <https://ruby-doc.org/core/Hash.html>`__ will list
their keys in a different order on every run to limit `algorithmic
complexity
attacks <http://perldoc.perl.org/perlsec.html#Algorithmic-Complexity-Attacks>`__.

The following Perl code will output the list in a different order on
every run:

.. container:: wrong

   {% highlight perl %} foreach my
   :math:`package (keys %deps) {  print MANIFEST, "`\ package:
   :math:`deps[`\ packages]“; } {% endhighlight %}

To get a deterministic output, the easiest way is to explicitly sort the
keys:

.. container:: correct

   {% highlight perl %} foreach my
   :math:`package (sort keys %deps) {  print MANIFEST, "`\ package:
   :math:`deps[`\ packages]“; } {% endhighlight %}

For Perl, it is possible to set ``PERL_HASH_SEED=0`` in the environment.
This will result in hash keys always being in the same order. See
`perlrun(1) <http://perldoc.perl.org/perlrun.html>`__ for more
information.

Python users can similarly set the environment variable
`PYTHONHASHSEED <https://docs.python.org/2/using/cmdline.html#envvar-PYTHONHASHSEED>`__.
When set to a given integer value, orders in dictionaries will be the
same on every run.

Beware that the [locale settings]({{ “/docs/locales/” \| relative_url
}}) might affect the output of some sorting functions or the ``sort``
command.
