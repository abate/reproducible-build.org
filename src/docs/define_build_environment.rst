
Define the build environment
============================

.. toctree::
   :maxdepth: 1

   perimeter
   definition_strategies
   recording
   proprietary_os

